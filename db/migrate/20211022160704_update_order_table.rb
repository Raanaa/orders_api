class UpdateOrderTable < ActiveRecord::Migration[6.1]
  def change
    change_column :orders, :status, :string, default: "pending"
  end
end
