require 'rails_helper'
RSpec.describe 'Users', type: :request do
    describe 'POST /register' do
      it 'authenticates the user' do
        post '/api/v1/register', params: { user: { first_name: 'user1', middle_name: 'middle_1' , last_name: 'last_1' , email: 'email' , phone_number: 'phone', password: 'password'  } }
        expect(response).to have_http_status(:created)
        expect(json).to eq({
                             'id' => User.last.id,
                             'first_name' => 'user1',
                             'middle_name' => 'middle_1',
                             'last_name' => 'last_1',
                             'email' => 'email',
                             'phone_number' => 'phone',
                             'token' => AuthenticationTokenService.call(User.last.id)
                           })
      end
    end
end