require 'rails_helper'

RSpec.describe "Orders", type: :request do
 
# initialize test data 
  let!(:orders) { create_list(:order, 10) }
  let(:order_id) { orders.first.id }
  #let(:user) { FactoryBot.create(:user, username: 'Rana', password: 'password') }
  let(:user) { FactoryBot.create(:user, first_name: 'user1', middle_name: 'middle_1' , last_name: 'last_1' , email: 'email' , phone_number: 'phone', password: 'password' ) }

  # Test GET /orders
  describe "GET /orders" do
    before { get '/api/v1/orders', headers: { 'Authorization' => AuthenticationTokenService.call(user.id) }}

    it 'returns orders' do
      #expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

   # Test GET /orders/:id
  describe 'GET /orders/:id' do
    before { get "/api/v1/orders/#{order_id}", headers: { 'Authorization' => AuthenticationTokenService.call(user.id) } }

    context 'when the record exists' do
      it 'returns the order' do
        #expect(json).not_to be_empty
        expect(json['id']).to eq(order_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:order_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match("{\"error\":\"Couldn't find Order with 'id'=100\"}")
      end
    end
  end

   # Test POST /orders
  describe 'POST /orders' do
    let(:valid_attributes) { { total_price: 100, status: 'accepted'} }

    context 'when the request is valid' do
      before { post '/api/v1/orders', params: valid_attributes , headers: { 'Authorization' => AuthenticationTokenService.call(user.id) }}

      it 'creates an order' do
        expect(json['total_price']).to eq(100)
        expect(json['status']).to eq('accepted')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/orders', params: { total_price: 100 },headers: { 'Authorization' => AuthenticationTokenService.call(user.id) } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match("{\"status\":[\"can't be blank\"]}")
      end
    end
  end

  # Test PUT /orders/:id
  describe 'PUT /orders/:id' do
    let(:valid_attributes) { { total_price: '1000' } }

    context 'when the record exists' do
      before { put "/api/v1/orders/#{order_id}", params: valid_attributes , headers: { 'Authorization' => AuthenticationTokenService.call(user.id) }}

      it 'updates the record' do
        expect(json).not_to be_empty
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  # Test DELETE /orders/:id
  describe 'DELETE /orders/:id' do
    
    context 'when the record deleted' do
      before { delete "/api/v1/orders/#{order_id}", headers: { 'Authorization' => AuthenticationTokenService.call(user.id) } }

      it 'delete the record' do
        expect(response.body)
          .to match("Order with id = #{order_id} is destroyed")
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end



end